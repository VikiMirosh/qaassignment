export class BookmarkedPage {

    visit() {
        cy.visit(Cypress.env('baseUrl') + 'feeds/bookmarked/')
        const title = cy.get('h1')
        title.should('have.text', 'Feeds')
    }

    verifyFeedAddedToBookmarked(url: string) {
        const page = cy.get('tbody')
        page.should('contain', url)
    }

    navigateToFeedPage(url: string) {
        const feedTitle = cy.contains(url).parent().children().first().find('[href^="/feeds/"]')
        feedTitle.click()
    }

    verifyFeedRemovedFromBookmarked(url: string) {
        cy.contains(url).should('not.exist')
    }
}