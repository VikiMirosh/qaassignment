// in cypress/support/index.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {

    interface Chainable {
      /**
       * Custom command to select DOM element by data-cy attribute.
       * @example cy.dataCy('greeting')
      */
      loginByCSRF(username: string, password: string): void
      createNewUser(username: string, password: string): void
      saveDateAndTime(): void
    }
  }