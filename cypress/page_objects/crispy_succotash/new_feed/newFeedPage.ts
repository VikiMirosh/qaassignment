export class NewFeedPage {

    enterUrl(url: string) {
        const passwordInput = cy.get('#id_feed_url')
        passwordInput.type(url)
    }

    submit() {
        const submitButton = cy.get('#submit-id-submit')
        submitButton.click()
    }
}