export class SignInPage {

    enterUsername(name: string) {
        const usernameInput = cy.get('#id_username')
        usernameInput.type(name)
    }

    enterPassword(password: string) {
        const passwordInput = cy.get('#id_password1')
        passwordInput.type(password)
    }

    confirmPassword(password: string) {
        const passwordInput = cy.get('#id_password2')
        passwordInput.type(password)
    }

    submit() {
        const submitButton = cy.get('#submit-id-submit')
        submitButton.click()
    }
}