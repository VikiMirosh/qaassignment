// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('loginByCSRF', (username, password) => {
  cy.request(Cypress.env('baseUrl') + 'accounts/login/')
    .then((response) => {
      const $html = Cypress.$(response.body)
      const csrf = $html.find('input[name="csrfmiddlewaretoken"]').attr('value')
      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrl') + 'accounts/login/',
        form: true,
        headers: {
          "X-CSRFToken": csrf
        },
        body: {
          "csrfmiddlewaretoken": csrf,
          "username": username,
          "password": password,
          "next": ""
        }
      }).as('loginUser')
        .then((resp) => {
          expect(resp.status).to.eq(200)
        })
    })
})

Cypress.Commands.add('createNewUser', (username, password) => {
  cy.request(Cypress.env('baseUrl') + 'accounts/register/')
    .then((response) => {
      const $html = Cypress.$(response.body)
      const csrf = $html.find('input[name="csrfmiddlewaretoken"]').attr('value')
      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrl') + 'accounts/register/',
        form: true,
        headers: {
          "X-CSRFToken": csrf
        },
        body: {
          "csrfmiddlewaretoken": csrf,
          "username": username,
          "password1": password,
          "password2": password,
          "submit": "Submit"
        }
      }).as('createNewUser')
        .then((resp) => {
          expect(resp.status).to.eq(200)
        })
    })
    cy.request(Cypress.env('baseUrl') + 'accounts/logout/')
})

Cypress.Commands.add('saveDateAndTime', () => {
  const initialDate = new Date().toLocaleString('en-us', {month: 'long', day: 'numeric', year: 'numeric', hour: 'numeric', minute: 'numeric', hour12: true, timeZone: 'Europe/London'})
  const finalDate = initialDate.replace(' at', ',').replace('PM', 'p.m.').replace('AM', 'a.m.')
  cy.wrap(finalDate).as('dateAndTime')
})  