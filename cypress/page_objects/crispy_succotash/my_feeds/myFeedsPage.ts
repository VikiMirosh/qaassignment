export class MyFeedsPage {

    visit() {
        cy.visit(Cypress.env('baseUrl') + 'feeds/my/')
        const title = cy.get('h1')
        title.should('have.text', 'Feeds')
    }

    verifyFeedExists(url: string) {
        const page = cy.get('tbody')
        page.should('contain', url)
    }
}