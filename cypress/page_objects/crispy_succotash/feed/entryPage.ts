export class EntryPage {

    writeComment(text: string) {
        const commentWindow = cy.get('.CodeMirror')
        commentWindow.click().type(text)
    }

    submitComment() {
        const submitButton = cy.get('#submit-id-submit')
        submitButton.click()
    }

    verifyCommentAddedNotification(message: string) {
        const notificationMessage = cy.get('.alert')
        notificationMessage.should('have.text', message)
    }

    verifyCommentVisible(text: string) {
        const commentsSection = cy.get('.col-sm-11').first()
        commentsSection.should('contain.text', text)
    }

    verifyCommentAuthor(text: string, name: string) {
        const commentAuthor = cy.contains(text).parent().parent().parent().children().first()
        commentAuthor.should('contain.text', name)
    }

    verifyCommentCreationTime(text: string) {
        const timeOfCreation = cy.contains(text).parent().parent().parent().children().first()
        timeOfCreation.should('contain.text', 'now')
    }

    navigateToFeedPage() {
        const feedTitle = cy.get('[class*="col-md-8"] > dl > dd > a').first()
        feedTitle.click()
    }
}