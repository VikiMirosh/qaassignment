import { HomePage } from "../../../page_objects/crispy_succotash/general/homePage";
import { SignInPage } from "../../../page_objects/crispy_succotash/general/signInPage";
import { AllFeedsPage } from "../../../page_objects/crispy_succotash/all_feeds/allFeedsPage";
import { LoginPage } from "../../../page_objects/crispy_succotash/general/loginPage";
const homePage = new HomePage
const signInPage = new SignInPage
const allFeedsPage = new AllFeedsPage
const loginPage = new LoginPage
const username = 'user_' + Date.now()
const password = Cypress.env('password')

describe('general signUp/login/logout functionality', () => {

    before(() => {
        cy.createNewUser(username, password)
    })

    it('sign up new user', () => {
        const username = 'user_' + Date.now()
        const password = Cypress.env('password')
        homePage.visit()
        homePage.navigateToSighInPage()
        signInPage.enterUsername(username)
        signInPage.enterPassword(password)
        signInPage.confirmPassword(password)
        signInPage.submit()
        allFeedsPage.verifyUserSignedUpNotification('Great success! Enjoy :)')
    })

    it('login user', () => {
        homePage.visit()
        homePage.navigateToLoginPage()
        loginPage.enterUsername(username)
        loginPage.enterPassword(password)
        loginPage.login()
        allFeedsPage.verifyUserLoggedIn()
    })

    it('logout user', () => {
        cy.loginByCSRF(username, password)
        allFeedsPage.visit()
        allFeedsPage.logoutUser()
        homePage.verifyUserLoggedOut()
    })
})