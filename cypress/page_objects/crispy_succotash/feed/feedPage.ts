export class FeedPage {

    verifyFeedAuthor(username: string) {
        const addedByField = cy.get('[class*="col-md-8"] > dl > dd').eq(0)
        addedByField.should('have.text', username)
    }

    verifyFeedUrl(url: string) {
        const feedUrlField = cy.get('[class*="col-md-8"] > dl > dd').eq(1)
        feedUrlField.should('have.text', url)
    }

    verifyDateAdded(date: string) {
        const dateAddedField = cy.get('[class*="col-md-8"] > dl > dd').eq(2)
        dateAddedField.should('have.text', date)
    }

    addFeedToBookmarked() {
        const heartButton = cy.get('[href$="/toggle-bookmark/"]')
        heartButton.click()
    }

    verifyHeartIconToBeColored() {
        const heartButton = cy.get('[href$="/toggle-bookmark/"]').children()
        heartButton.should('have.class', 'glyphicon glyphicon-heart')
    }

    verifyHeartIconToBeEmpty() {
        const heartButton = cy.get('[href$="/toggle-bookmark/"]').children()
        heartButton.should('have.class', 'glyphicon glyphicon-heart-empty')
    }

    removeFeedFromBookmarked() {
        const heartButton = cy.get('[href$="/toggle-bookmark/"]')
        heartButton.click()
    }

    saveFeedFirstEntry() {
        cy.get('tbody > tr > td > a').first().invoke('text').as('firstEntry')
    }

    checkFeedForUpdates() {
        const checkForUpdatesButton = cy.get('[href$="/update/"]')
        checkForUpdatesButton.click()
    }

    verifyUpdates(successMessage: string, date: string, noUpdatesMessage: string, text: string) {
        const alert = cy.get('.alert')
        .then(notificationMessage => {
            if (notificationMessage.text().includes(successMessage)) {
                alert.should('have.text', successMessage)
                
                const lastCheckedField = cy.get('[class*="col-md-8"] > dl > dd').last()
                lastCheckedField.should('have.text', date)
                
                const feedFirstEntry = cy.get('tbody > tr > td > a').first()
                feedFirstEntry.should('not.have.text', text)
            } else {
                alert.should('have.text', noUpdatesMessage)
                
                const feedFirstEntry = cy.get('tbody > tr > td > a').first()
                feedFirstEntry.should('have.text', text)
            }
        })
    }

    saveNumberOfComments() {
        cy.get('tbody').children().first().children().last().invoke('text').as('numberOfComments')
    }

    navigateToEntryPage() {
        const firstEntry = cy.get('tbody > tr > td > a').first()
        firstEntry.click()
    }

    verifyNumberOfCommentsChange(initialNumber: string) {
        const expectedNumber = Number(initialNumber) + 1
        const numberOfComments = cy.get('tbody').children().first().children().last()
        numberOfComments.should('have.text', expectedNumber)
    }
}