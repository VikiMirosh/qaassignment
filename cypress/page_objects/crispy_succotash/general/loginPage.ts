export class LoginPage {

    enterUsername(name: string) {
        const usernameInput = cy.get('#id_username')
        usernameInput.type(name)
    }

    enterPassword(password: string) {
        const passwordInput = cy.get('#id_password')
        passwordInput.type(password)
    }

    login() {
        const loginButton = cy.get('[value="login"]')
        loginButton.click()
    }
}