export class HomePage {

    visit() {
        cy.visit(Cypress.env('baseUrl'))
        const title = cy.get('.name')
        title.should('have.text', 'TEST ME.')
    }

    navigateToSighInPage() {
        const signUpButton = cy.get('[href*="/accounts/register/"]')
        signUpButton.click()
        const title = cy.get('.panel-title')
        title.should('contain.text', 'Sign Up')
    }

    navigateToLoginPage() {
        const signUpButton = cy.get('[href*="/accounts/login/"]')
        signUpButton.click()
        const title = cy.get('.panel-heading')
        title.should('contain.text', 'Please Sign In')
    }

    verifyUserLoggedOut() {
        cy.contains('Login')
    }
}