export class AllFeedsPage {

    visit() {
        cy.visit(Cypress.env('baseUrl') + 'feeds/')
        const title = cy.get('h1')
        title.should('have.text', 'Feeds')
    }

    verifyUserSignedUpNotification(message: string) {
        const notificationMessage = cy.get('.alert')
        notificationMessage.should('have.text', message)
    }

    verifyUserLoggedIn() {
        cy.contains('Logout')
    }

    logoutUser() {
        const logoutButton = cy.get('[href*="/accounts/logout/"]')
        logoutButton.click()
    }

    verifyFeedExists(url: string) {
        const page = cy.get('tbody')
        page.should('contain', url)
    }

    navigateToFeedPage(url: string) {
        const feedTitle = cy.contains(url).parent().children().first().find('[href^="/feeds/"]')
        feedTitle.click()
    }

    saveFeedId(url: string) {
        cy.contains(url).parent().children().first().find('[href^="/feeds/"]').invoke('attr', 'href').as('feedId')
    }

    navigateToAddNewFeedPage() {
        const newFeedButton = cy.get('[href*="/feeds/new/"]')
        newFeedButton.click()
    }
}