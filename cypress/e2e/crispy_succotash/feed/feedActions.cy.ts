import { AllFeedsPage } from "../../../page_objects/crispy_succotash/all_feeds/allFeedsPage";
import { FeedPage } from "../../../page_objects/crispy_succotash/feed/feedPage";
import { BookmarkedPage } from "../../../page_objects/crispy_succotash/bookmarked/bookmarkedPage";
import { FeedRequests } from "../../../backend_calls/crispy_succotash/feedRequests";
import { NewFeedPage } from "../../../page_objects/crispy_succotash/new_feed/newFeedPage";
import { MyFeedsPage } from "../../../page_objects/crispy_succotash/my_feeds/myFeedsPage";
import { EntryPage } from "../../../page_objects/crispy_succotash/feed/entryPage";
const entryPage = new EntryPage
const allFeedsPage = new AllFeedsPage
const feedPage = new FeedPage
const bookmarkedPage = new BookmarkedPage
const feedRequests = new FeedRequests
const newFeedPage = new NewFeedPage
const myFeedsPage = new MyFeedsPage
const feedUrl = Cypress.env('feedUrl1')
const username = 'user_' + Date.now()
const password = Cypress.env('password')
const successMessage = 'Found new updates. Enjoy!'
const noUpdatesMessage = 'Nothing new yet...'
const commentText = 'Test_comment_' + Date.now()
const commentAddedNotification = 'Comment added successfully'

describe('feed functionality', () => {
  
    before(() => {
        cy.createNewUser(username, password)
    })

    beforeEach(() => {
        cy.loginByCSRF(username, password)
        allFeedsPage.visit()
    })

    it('add new feed', function () {
        allFeedsPage.navigateToAddNewFeedPage()
        newFeedPage.enterUrl(feedUrl)
        newFeedPage.submit()
        cy.saveDateAndTime()
        feedPage.verifyFeedAuthor(username)
        feedPage.verifyFeedUrl(feedUrl)
        cy.then(() => {
            feedPage.verifyDateAdded(this.dateAndTime)
            allFeedsPage.visit()
            allFeedsPage.verifyFeedExists(feedUrl)
            myFeedsPage.visit()
            myFeedsPage.verifyFeedExists(feedUrl)
        })
    })

    it('add feed to bookmarked', function () {
        allFeedsPage.saveFeedId(feedUrl)
        allFeedsPage.navigateToFeedPage(feedUrl)
        feedPage.addFeedToBookmarked()
        feedPage.verifyHeartIconToBeColored()
        bookmarkedPage.visit()
        bookmarkedPage.verifyFeedAddedToBookmarked(feedUrl)
        cy.then(() => {
            feedRequests.removeFeedFromBookmarked(this.feedId)
        })
    })
    
    it('remove feed from bookmarked', function () {
        allFeedsPage.saveFeedId(feedUrl)
        cy.then(() => {
            feedRequests.addFeedToBookmarked(this.feedId)
            bookmarkedPage.navigateToFeedPage(feedUrl)
            feedPage.removeFeedFromBookmarked()
            feedPage.verifyHeartIconToBeEmpty()
            bookmarkedPage.visit()
            bookmarkedPage.verifyFeedRemovedFromBookmarked(feedUrl)
        })
    })

    it('check feed for updates', function () {
        allFeedsPage.navigateToFeedPage(feedUrl)
        feedPage.saveFeedFirstEntry()
        feedPage.checkFeedForUpdates()
        cy.saveDateAndTime()
        cy.then(() => {
            feedPage.verifyUpdates(successMessage, this.dateAndTime, noUpdatesMessage, this.firstEntry)
        })
    })

    it('write a comment inside entry', function () {
        cy.loginByCSRF(username, password)
        allFeedsPage.visit()
        allFeedsPage.navigateToFeedPage(feedUrl)
        feedPage.saveNumberOfComments()
        feedPage.navigateToEntryPage()
        entryPage.writeComment(commentText)
        entryPage.submitComment()
        entryPage.verifyCommentAddedNotification(commentAddedNotification)
        entryPage.verifyCommentVisible(commentText)
        entryPage.verifyCommentAuthor(commentText, username)
        entryPage.verifyCommentCreationTime(commentText)
        entryPage.navigateToFeedPage()
        cy.then(() => {
            feedPage.verifyNumberOfCommentsChange(this.numberOfComments)
        })
    })
})