export class FeedRequests {

    addFeedToBookmarked(feedId: string) {
        cy.request({
            method: 'GET',
            url: Cypress.env('baseUrl') + 'feeds/' + feedId.replace('/feeds/', '') +  '/toggle-bookmark/',
            headers: {
            }
        }).as('addFeedToBookmarked')
            .then((resp) => {
                expect(resp.status).to.eq(200)
            })
    }

    removeFeedFromBookmarked(feedId: string) {
        cy.request({
            method: 'GET',
            url: Cypress.env('baseUrl') + 'feeds/' + feedId.replace('/feeds/', '') +  '/toggle-bookmark/',
            headers: {
            }
        }).as('removeFeedFromBookmarked')
            .then((resp) => {
                expect(resp.status).to.eq(200)
            })
    }
}